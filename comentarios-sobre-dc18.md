# Comentários gerais sobre DC18


## Alimentação

Aqui não tem uma mesa de legumes e verduras. Isso fará diferença no nosso buffet.

## Conteúdo

O Gunnar me explicou que o time de conteúdos é composto por 6 pessoas, e eles dão notas para as propostas. Essas notas vão de -2 a +2, e normalmente quem fica acima de 1 é selecionado.

O sistema não é muito legal pra montar a programação, então ele monta numa planilha e depois passar para o sistema.

## Vídeo

https://debconf-video-team.pages.debian.net/docs/room_setup.html#video

## FrontDesk

Local escolhido é muito pequeno e não é possível manter organizado. 

Fica difícil de manter um fluxo no atendimento.


## Debian Store

Pessoal gostou muito de ter produtos de qualidade do Debian..

## Accesibilidade

Alguns lugares são dificil de acesso para quem possui necessidades especiais de locomoção. 
Ex. Ir do Dormitório ate o Venue, tem que ir pela rua.

Ir até o restaurante, tem que atravessar locais impossíveis para um cadeirante, que tem que dar uma volta bem maior.

Palestrante cadeirante terá problema.

