# Empresas brasileiras

Empresas brasileiras para enviar o projeto de patricínio:

## Nacionais

* 4Linux (São Paulo)
* Async Open Source (São Paulo)
* Editora Novatec (SãoPaulo)
* Globo.com (Rio de Janeiro)
* Grupo Utah (São Paulo)
* iMasters (São Paulo)
* KHOMP (Florianópolis)
* Kinghost (Porto Alegre)
* Locaweb (São Paulo)
* Nic.br (São Paulo)
* OpenS (Florianópolis)
* O.S. Systems (Pelotas)
* Propus (Porto Alegre)
* Rocket.Chat (Porto Alegre)
* Tempo Real Eventos (São Paulo)
* Terra (São Paulo)
* ThoughtWorks (São Paulo)

## Locais

* Ambiente Livre Tecnologia
* Assespro
* Cinq
* Ebanx
* Elaborata Informática
* Hop'n Roll Beer Club
* BrodeBrown
* Intelbras
* iSolve
* Livrarias Curitiba
* MPS Informática
* Pelissari
* Positivo Informática
* Sucesu
* Tuxnet IT Solutions
* Wise Systems
* WSU

## Públicas

* Celepar
* DataPrev
* Fenadados
* Itaipu
* Prefeitura de Curitiba
* Serpro
* Governo do Paraná