**This is just a draft of expenses list.**

# Lista de prováveis despesas

Insira aqui tudo o que você acredita que deveremos pagar.
Não se preocupe se algo não estiver aprovado ainda, se for preciso depois
removemos.

## Child care

 * Child care

## Accommodation - hotel

 * Bedrooms
 * Cleaning 13o. floor
 * Food and drink for vegan on hotel breakfest

## Catering - RU

 * Lunch
 * Dinner
 * Juice

## Conference dinner

 * Food
 * Rent space
 * Transport

## Content

 * Invited speakers for Open Day

## Day trip

 * Food
 * Transport

## Food and drinks

 * Coffee-break
 * Coffee and Tea
 * Sugar
 * Cups
 * Plastic cup

## Hackerspace at hotel

 * Bar beers
 * Snacks

## Cheese and Wine

 * Food
 * Drinks
 * Rent space
 * Transport

## Swags (kit participante)

 * Badge
 * Bag
 * Sticker
 * Lanyard
 * Cup
 * Brinde local

## T-shirt

 * Attendee
 * Staff
 * Video team
 * Welcome team

## Video

 * Equipment Rental
 * Equipment Shipping

## Venue

 * Staff from UTFPR to work rooms

## Material gráfico

 * Cartaz A3
 * Banners de lona
    * Auditórios e salas
    * Sinalização
    * Grande com todas as logos
    * Entrada

## Seguro e/ou ambulância

 * Seguro
 * Ambulância para emergências
